# trunk-based-kata


La idea de esta kata es poder aprender las bases de trunk based develompent. Para eso, se deberá realizar la siguiente ejercitación con los siguientes constraints.

### Ejercitacion

Durante el último mes se estuvo desarrollando un nuevo juego estrategia llamado Civilization & Conquer, y ahora que es un exito el equipo decidió hacer un cambio en cómo trae el nivel. Actualmente, sólo trae un nivel que está creado en el propio código lo que imposibilita la nueva creación de niveles. La nueva idea es que traiga los niveles de un scriptable object en resources en base al número de nivel que se pida. Por si fuera poco, los inversionistas quieren  que se prepare el codigo para que en el futuro se pueda cargar niveles desde fuera de la app y no tener que depender de un nuevo deploy de cliente. 

En resumen, lo necesario es:
- Poder cargar los niveles desde resources
- Poder elegir el nivel en base al número de ID.
- Hacer que sea asincrónico para el futuro.

Para poder cumplir con CI/CD, imaginaremos que siempre que pusheamos estamos mandando a producción el juego, así que hay un test que ya viene hecho el cual SIEMPRE tiene que pasar. Si ese test falla antes de que el feature esté completo significaría que hay usuarios que no pueden cargar un nivel correctamente.

### Constraints

- No se puede crear código productivo sin TDD.
- Se tiene que hacer un commit y pushear por cada test creado/modificado. Nada de commits con muchos cambios.
- NUNCA puede pushearse algo con algun test fallando. Hay que ingenearselas para poder pushear cada pequeño cambio sin que falle el pipeline. _Pista: es probable que necesiten crear un servicio de feature toggle _
