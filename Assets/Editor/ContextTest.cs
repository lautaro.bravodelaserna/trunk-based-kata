﻿using NUnit.Framework;
using UnityEngine;

namespace Editor
{
    public class ContextTest
    {

        [Test]
        public void get_level_from_level_service()
        {
            var gameObject = new GameObject();
            var context = gameObject.AddComponent<Context>();
            context.Start();
            Assert.AreEqual("Level: 1 - This is the first level - |Gold x 100|Sword x 1|", context.level.ToString());
        }
    }
}
