using System.Collections.Generic;

public class LocalLevelRepository
{
    public virtual Level Get()
    {
        return new Level(1, "This is the first level", new Rewards(new List<Reward>
        {
            new Reward("Gold", 100),
            new Reward("Sword", 1)
        }));
    }
}