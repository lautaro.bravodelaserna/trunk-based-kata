﻿using UnityEngine;

public class Context : MonoBehaviour
{
    public Level level;
    
    public void Start()
    {
        LocalLevelRepository localLevelRepository = new LocalLevelRepository();
        var levelService = new LevelService(localLevelRepository);

        level = levelService.GetLevel();
        Debug.Log(level);
    }
}