public class ToggleService : IToggleService
{
    private const bool NewRefactorEnabled = false;
    
    public bool IsNewRefactorEnabled()
    {
        return NewRefactorEnabled;
    }
}