public class Reward
{
    private readonly string _itemId;
    private readonly int _amount;

    public Reward(string itemId, int amount)
    {
        _itemId = itemId;
        _amount = amount;
    }

    public override string ToString() => $"{_itemId} x {_amount}";
}