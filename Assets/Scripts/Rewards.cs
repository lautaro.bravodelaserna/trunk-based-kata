using System.Collections.Generic;
using System.Linq;

public class Rewards
{
    private readonly List<Reward> _rewards;

    public Rewards(List<Reward> rewards)
    {
        _rewards = rewards;
    }

    public override string ToString() => _rewards.Aggregate("|", (seed, reward) => seed + reward + "|");
}