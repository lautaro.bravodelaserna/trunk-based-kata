using UnityEngine;

[CreateAssetMenu(menuName = "Create Level", fileName = "Level", order = 0)]
public class Level : ScriptableObject
{
    private readonly int _levelId;
    private readonly string _description;
    private readonly Rewards _rewards;

    public Level(int levelId, string description, Rewards rewards)
    {
        _levelId = levelId;
        _description = description;
        _rewards = rewards;
    }

    public override string ToString() => $"Level: {_levelId} - {_description} - {_rewards}";
}