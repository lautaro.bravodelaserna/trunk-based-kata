public class LevelService
{
    private readonly LocalLevelRepository _localLevelRepository;

    public LevelService(LocalLevelRepository localLevelRepository)
    {
        _localLevelRepository = localLevelRepository;
    }

    public Level GetLevel()
    {
        return _localLevelRepository.Get();
    }
}